Docker-Image: [Docker-speedtest](https://hub.docker.com/r/henrywhitaker3/speedtest-tracker)


## Intro

I came across henrywhitaker3 (link above) speedtest container as I was in desperate need of monitoring my internet speeds as i was debating with my ISP about the speeds provided. Keep in mind, I pay for business class cable internet, with a noticable difference in contracts as business class offers SLA's and "normally" higher priority on the wire. A biggy for me. Anyway, found henry's dockers image.  The goal was have a test spun up every 5 minutes and provide basic level of authentication and this one seemed to do that. 

 ## Choosing your Operating System
 
 Here is the tricky part that most folks will run through. Which OS to use? and more importantly, will we use the HOST OS/baremetal to manage the docker container, OR spin up a VM on our preferred virtualization host? Well, in this use case, i have a NAS device that runs [OpenMediaVault](https://www.openmediavault.org/) , which is basically Debian  that also acts as a docker system. Making it barebones and isolated from most of my home infrastructure makes this a pretty easy setup (yes, it need's external access for this speedtest container to be useful).  
 
 ## Build
  
 - Host OS: [openmediavault *.*](https://www.openmediavault.org/)
- Build: 
	- CPU: 8 core
	- Memory: 16 Gig
	- Storage: ALOT Gig
	- Network: Bridge-Static IP
		- please issue a static ip

## After OS steps

None. the OS is already on the box and install is really for another git repo.

## Speedtest install

We are ready to go. cd to the folder that you will be dumping your repo's to. Example:
```
cd /home/(user)/projects/
```
Clone the template attached to this [git](https://gitlab.com/smiley.j0k3r/docker-speedtest.git).
```
git clone https://gitlab.com/smiley.j0k3r/docker-speedtest.git
```
Once you completed the cloning, open it up. Now, HenryWhitaker has further options which are mentioned in his docker and github sites, but I've made some simple changes that fit what Im trying to do.
```
nano docker-compose.yml
```
NOW, assuming this MAY be on a docker system that has multiple containers running, lets assume we need a close eye on the ports being used in the compose file. The default values I have here are:
```
        - ports:
            - 9002:80
```
Luckly, this only has 1 defined listening port, which happens to be the webGUI. Simple. Now the port 9002 is the external port that we will connect to view the page. This value can be changed to whatever you'd like.
Next we will look at the volumes section.
```
        - volumes:
            - /srv/ENTERLOCALORNASSSID/configs/speedtest:/config
```
I personally keep all my configs and on my homelab NAS. but if you are considering in leaving local to the container, we would change it up alittle. 
```
        volumes:
            - ./path/to/data:/config
            - EXAMPLE: ./config/:/config
```
this indicates we are going local instead of using a external storage source. there are positive and negatives for each, but that is widely debated for each use case that others on youtube have made. 



